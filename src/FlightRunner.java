/**
 * 
 * Flight Runner Class
 * @author Vincent Moeykens
 * Created for CCV Java Class, Fall 2016
 * 
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

public class FlightRunner {

	public static void main(String[] args) {
		// Scanner
		Scanner s = new Scanner(System.in);
		// Prompt for how many flights are being added
		System.out.println("How many flights would you like to add?");
		// Get selection
		int amountOfFlights = s.nextInt();
		s.nextLine();
		// Loop adding flights for the amount of time selected
		for (int x = 0; x < amountOfFlights; x++) {
			addNewFlight();
		}
		System.out.println("\nDone!\n");
		// Close Scanner
		s.close();
	}

	// Method for getting all flight information and adding it to a file
	public static void addNewFlight() {
		// Create new scanner object
		Scanner s = new Scanner(System.in);
		// Object to store final flight info
		Flight newFlight = new Flight();

		/**
		 * Get basic flight information:
		 */

		// Determine if Glider or Powered
		System.out.println("Was the aircraft a glider or powered?\n1 - Glider\n2 - Powered");
		// If aircraft is a glider then set aircraft type and set aerotow to
		// true
		if (s.nextInt() == 1) {
			// Capture final \n input from nextInt()
			s.nextLine();
			// Prompt for basic information
			// Starting Airport
			System.out.println("Please enter your starting airport:");
			String startAirport = s.nextLine();
			// Ending airport
			System.out.println("Please enter your ending airport:");
			String endAirport = s.nextLine();
			// Flight time
			System.out.println("Please enter the flight time in hours:");
			double flightTime = s.nextDouble();
			// Carrying passengers or not
			boolean carryingPassengers = false;
			System.out.println("Were you carrying passengers?\n1 - Yes\n2 - No");
			if (s.nextInt() == 1) {
				carryingPassengers = true;
			} else {
				carryingPassengers = false;
			}
			Flight gliderFlight = new Flight(startAirport, endAirport, flightTime, carryingPassengers, "Glider", true);
			newFlight = gliderFlight;
		} else {
			// Capture final \n input from nextInt()
			s.nextLine();
			// Prompt for basic information
			// Starting Airport
			System.out.println("Please enter your starting airport:");
			String startAirport = s.nextLine();
			// Ending airport
			System.out.println("Please enter your ending airport:");
			String endAirport = s.nextLine();
			// Flight time
			System.out.println("Please enter the flight time in hours:");
			double flightTime = s.nextDouble();
			// Carrying passengers or not
			boolean carryingPassengers = false;
			System.out.println("Were you carrying passengers?\n1 - Yes\n2 - No");
			if (s.nextInt() == 1) {
				carryingPassengers = true;
			} else {
				carryingPassengers = false;
			}
			s.nextLine();
			// Prompt for aircraft type
			System.out.println("Please enter aircraft type:");
			String aircraftType = s.nextLine();
			Flight poweredFlight = new Flight(startAirport, endAirport, flightTime, carryingPassengers, aircraftType,
					false);
			newFlight = poweredFlight;
		}

		/**
		 * Begin loop to allow changing and finalizing of information
		 */
		// Variable for option selection loop
		int selectedOption = 3;
		// Begin loop
		while (selectedOption >= 2 && selectedOption <= 7) {
			// Prompt user
			System.out.println(
					"Please Choose a Command:\n0 - Exit\n1 - Write To Log (When finished with flight data)\n2 - Set Airports\n3 - Set Flight Time\n4 - Set Aircraft Type\n5 - Set if carrying passengers\n6 - Print Flight Information to console\n7 - List Aircraft Types");
			// Get Option
			selectedOption = s.nextInt();
			s.nextLine();
			// Switch for selected options
			switch (selectedOption) {
			case 1:
				// Do nothing
				break;
			case 2:
				// Prompt for airports and assign them
				System.out.print("Enter Start Airport: ");
				newFlight.startAirport = s.nextLine();
				System.out.print("Enter End Airport: ");
				newFlight.endAirport = s.nextLine();
				break;
			case 3:
				// Prompt for flight times and assign them
				System.out.print("Enter Flight Time in Hours: ");
				newFlight.flightTime = s.nextDouble();
				s.nextLine();
				break;
			case 4:
				// Prompt for aircarft type and assign it
				System.out.print("Enter Aircraft Type: ");
				newFlight.aircraftType = s.nextLine();
				break;
			case 5:
				// Prompt for passengers and assign it
				System.out.print("Were you carrying passengers?\n1 - Yes\n2 - No");
				if (s.nextInt() == 1) {
					s.nextLine();
					newFlight.carryingPassengers = true;
				} else {
					newFlight.carryingPassengers = false;
				}
				break;
			case 6:
				System.out.println("\nFlight information:\nStart and End Airports: " + newFlight.startAirport + " --> "
						+ newFlight.endAirport + "\nFlight Time: " + newFlight.flightTime + " hours.\nAircraft Type: "
						+ (String) newFlight.aircraftType + "\nCarrying Passengers? " + newFlight.carryingPassengers
						+ "\nAerotow? " + newFlight.aerotow + "\nFlight Distance: " + newFlight.getDistanceFlown()
						+ "\n");
				break;
			case 7:
				System.out.println("\n");
				Flight.listAircraftType();
				System.out.println("\n");
				break;
			default:
				break;

			}

		}

		if (selectedOption == 1) {
			/**
			 * 
			 * NOTE: BufferedWriter and FileWriter are used to allow for the
			 * file to be written without overwriting data already there
			 * 
			 */
			try {
				// Create a new file object, and then check if a file exists or
				// not already
				File file = new File("flightLog.txt");
				if (!file.exists()) {
					file.createNewFile();
				}
				// Create a new printwriter object with nested bufferedwriter
				// and filewriter
				PrintWriter w = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
				// Print information to file
				w.println("Flight information || Start and End Airports: " + newFlight.startAirport + " --> "
						+ newFlight.endAirport + " || Flight Time: " + newFlight.flightTime
						+ " hours. || Aircraft Type: " + (String) newFlight.aircraftType + " || Carrying Passengers? "
						+ newFlight.carryingPassengers + " || Aerotow? " + newFlight.aerotow + " || Flight Distance: "
						+ newFlight.getDistanceFlown());
				// Add blank line to give space
				w.println("");
				// Close the Printwriter
				w.close();
			} catch (Exception e) {
				// Catch Errors
			}
		} else if (selectedOption == 0) {
			// Quit
		}
		// Close Scanner
		// s.close();
	}
}

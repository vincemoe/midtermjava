
/**
 * 
 * Flight Class, used for creating a flight object
 * @author Vincent Moeykens
 * Created for CCV Java Class, Fall 2016
 * 
 */
import java.util.Scanner;

public class Flight {

	/**
	 * 
	 * Assigning base arrays for holding a list of "known" airports with
	 * arbitrary coordinates and a list of aircraft type
	 * 
	 */
	// Array of airports and corresponding coordinates
	public static Airport airportList[] = { new Airport("KVSF", 0, 0, "Hartness State Airport"),
			new Airport("KLEB", 0, 5, "Lebanon Municipal Airport"), new Airport("KEEN", 3, -5, "Keene State Airport"),
			new Airport("KMPV", -4, 2, "Knapp State Airport/Montpelier"),
			new Airport("KBOS", 5, -10, "Boston International"),
			new Airport("KBTV", -3, 4, "Burlington International") };

	// Array of aircraft type
	public static String aircraftTypeList[] = { "Glider", "Single Engine Land", "Single Engine Sea", "Dual Engine Land",
			"Dual Engine Sea", "Jet", "High Performance" };

	/**
	 * 
	 * Set base variables for constructor
	 * 
	 */
	// Strings for starting airport, ending airport, and aircraft type
	String startAirport, endAirport, aircraftType;
	// Double for the length of the flight
	double flightTime;
	// Boolean for if carrying passengers or not
	boolean carryingPassengers;
	// Boolean for if aero-tow or not (only glider)
	boolean aerotow;

	/**
	 * 
	 * Basic constructors, overloaded to allow a constructor with no parameters
	 * 
	 * @param startAirport
	 * @param endAirport
	 * @param flightTime
	 * @param carryingPassengers
	 * @param aircraftType
	 * @param aerotow
	 */

	// Basic overloaded constructor
	public Flight(String startAirport, String endAirport, double flightTime, boolean carryingPassengers,
			String aircraftType, boolean aerotow) {
		// Assign variables using the this keyword

		// Check validity of airport before assigning
		if (isValidAirport(startAirport)) {
			this.startAirport = startAirport;
		} else {
			this.startAirport = null;
			// System.out.println("Warning! You entered an invalid starting
			// airport, value set to null!");
		}
		// Check validity of airport before assigning
		if (isValidAirport(endAirport)) {
			this.endAirport = endAirport;
		} else {
			this.endAirport = null;
			// System.out.println("Warning! You entered an invalid ending
			// airport, value set to null!");
		}
		this.flightTime = flightTime;
		this.carryingPassengers = carryingPassengers;
		this.aircraftType = aircraftType;
		// If it is a glider, assign the inputted value for aero-tow, if it
		// isn't
		// a glider, it can not have been aero-towed
		if (aircraftType == "Glider" || aircraftType == "glider") {
			this.aerotow = aerotow;
		} else {
			this.aerotow = false;
		}
	}

	// Default constructor
	public Flight() {
		// Create a new flight with default values
		this(null, null, 0, false, null, false);
	}

	/**
	 * 
	 * "Setter Methods", allowing the user to change or set the values of a
	 * flight
	 * 
	 */

	// Method for setting the airports flown to
	public void setAirports(String startAirport, String endAirport) {
		// Check for validity and assign variables
		if (isValidAirport(startAirport)) {
			this.startAirport = startAirport;
		} else {
			this.startAirport = null;
			System.out.println("Warning! You entered an invalid starting airport, value set to null!");
		}
		// Check for validity and assign variables
		if (isValidAirport(endAirport)) {
			this.endAirport = endAirport;
		} else {
			this.endAirport = null;
			System.out.println("Warning! You entered an invalid ending airport, value set to null!");
		}
	}

	// Set flight time
	public void setFlightTime(double flightTime) {
		this.flightTime = flightTime;
	}

	// Set aircraft type, you should list aircraft type before using this method
	// so user knows the options
	public void setAirplaneType(String aircraftType) {
		if (isValidAircraftType(aircraftType)) {
			this.aircraftType = aircraftType;
		} else {
			System.out.println("Invalid Aircraft Type! Setting to null");
			this.aircraftType = null;
		}
	}

	// Set whether or not you were carrying passengers
	public void setPassengers(boolean wasCarryingPassengers) {
		this.carryingPassengers = wasCarryingPassengers;
	}

	/**
	 * 
	 * Basic "Getter" Methods
	 * 
	 */

	// Get x value of inputted airport
	private double getAirportX(String airport) {
		double airportX = 0;
		for (int x = 0; x < airportList.length; x++) {
			if (airport.equals(airportList[x].getAirportICAOCode())) {
				airportX = airportList[x].xLoc;
			}
		}
		return airportX;
	}

	// Get y value of inputted airport
	private double getAirportY(String airport) {
		double airportY = 0;
		for (int x = 0; x < airportList.length; x++) {
			if (airport.equals(airportList[x].getAirportICAOCode())) {
				airportY = airportList[x].yLoc;
			}
		}
		return airportY;
	}

	// Get aircraft type
	public String getAircraftType() {
		return this.aircraftType;
	}

	// Get if was carrying passengers
	public boolean wasCarryingPassengers() {
		return this.carryingPassengers;
	}

	// Use distance formula and methods for getting airport x and y values to
	// calculate the "distance" (NOTE, X and Y values are assigned to arbitrary
	// numbers, and are simply for demonstration and have no basis in accurate
	// real world measurements)
	public double getDistanceFlown() {
		if (this.startAirport != null && this.endAirport != null) {
			return Math.sqrt(Math.pow((getAirportX(this.endAirport) - getAirportX(this.startAirport)), 2)
					+ Math.pow((getAirportY(this.endAirport) - getAirportY(this.startAirport)), 2));

		}
		return 0;
	}

	/**
	 * 
	 * Validating methods, used for determining validity of an object/string
	 * 
	 */

	// Boolean for comparing an inputted value to the array of airports to check
	// for validity
	private static boolean isValidAirport(String airportTest) {
		for (int x = 0; x < airportList.length; x++) {
			if ((airportList[x].getAirportICAOCode()).equals(airportTest)) {
				return true;
			}
		}
		return false;
	}

	// Boolean for comparing an inputted value to the array of aircraft type to
	// check
	// for validity
	private static boolean isValidAircraftType(String aircraftType) {
		for (int x = 0; x < aircraftTypeList.length; x++) {
			if (aircraftTypeList[x].equals(aircraftType)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * Misc Methods
	 * 
	 */

	// Method for adding a new airport
	public static void enterNewAirport() {
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the ICAO code of the new airport: ");
		String newAirport = s.nextLine();
		System.out.print("Enter the X coordinate of the new airport: ");
		int newAirportX = s.nextInt();
		System.out.print("Enter the Y coordinate of the new airport: ");
		int newAirportY = s.nextInt();
		s.close();
		System.out
				.println("Your new airport is " + newAirport + ". It is at " + newAirportX + ", " + newAirportY + ".");
	}

	public static void listAirports() {
		// Loop through list and print airports, method is mainly for debugging
		for (int x = 0; x < airportList.length; x++) {
			// - Left justified, 40 is how many characters width, %s is any
			// string
			String format = "%-40s%s%n";
			System.out.printf(format, airportList[x].airportName, airportList[x].icaoCode);
			// Old format using \t for tab
			// System.out.println(airportList[x].icaoCode + "\t:\t" +
			// airportList[x].airportName);
		}
	}

	public static void listAircraftType() {
		// Loop through list and print airports, method is mainly for debugging
		for (int x = 0; x < aircraftTypeList.length; x++) {
			// - Left justified, 40 is how many characters width, %s is any
			// string
			System.out.println(aircraftTypeList[x]);
			// Old format using \t for tab
			// System.out.println(airportList[x].icaoCode + "\t:\t" +
			// airportList[x].airportName);
		}
	}

}

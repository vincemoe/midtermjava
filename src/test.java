
public class test {
	public static void main(String[] args) {

		System.out.println(answer("Vincent"));
	}

	public static String answer(String plaintext) {
		// Vincent Moeykens
		String cap = "000001";
		String outputString = "";
		char eng[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
				't', 'u', 'v', 'w', 'x', 'y', 'z', ' ' };
		String braille[] = { "100000", "110000", "100100", "100110", "100010", "110100", "110110", "110010", "010100",
				"010110", "101000", "111000", "101100", "101110", "101010", "111100", "111110", "111010", "011100",
				"011110", "101001", "111001", "010111", "101101", "101111", "101011", "000000" };

		for (int x = 0; x < plaintext.length(); x++) {
			for (int y = 0; y < eng.length; y++) {

				if (eng[y] == plaintext.toLowerCase().charAt(x) && Character.isUpperCase(plaintext.charAt(x))) {
					// System.out.print(cap);
					outputString = outputString.concat(cap);
					// System.out.print(braille[y]);
					outputString = outputString.concat(braille[y]);
				} else if (eng[y] == plaintext.charAt(x)) {
					// System.out.print(braille[y]);
					outputString = outputString.concat(braille[y]);
				} else {

				}
			}
		}
		return outputString;
	}
}
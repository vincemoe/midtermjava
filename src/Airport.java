/**
 * 
 * Class to hold airport information
 * 
 * @author Vincent Moeykens Created for CCV Java Class, Fall 2016
 *
 */
public class Airport {
	// Base variables
	public String airportName;
	public String icaoCode;
	public double xLoc;
	public double yLoc;

	// Default and overloaded constructors
	public Airport(String icaoCode, double xLoc, double yLoc) {
		this.icaoCode = icaoCode;
		this.xLoc = xLoc;
		this.yLoc = yLoc;
		this.airportName = null;
	}

	public Airport(String icaoCode, double xLoc, double yLoc, String airportName) {
		this.icaoCode = icaoCode;
		this.xLoc = xLoc;
		this.yLoc = yLoc;
		this.airportName = airportName;
	}

	public Airport() {
		this(null, 0, 0, null);
	}

	// "Getter" methods
	public String getAirportName() {
		return this.airportName;
	}

	public String getAirportICAOCode() {
		return this.icaoCode;
	}

	public double getAirportX() {
		return this.xLoc;
	}

	public double getAirportY() {
		return this.yLoc;
	}

	// "Setter" methods
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public void setAirportICAOCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public void setAirportX(double x) {
		this.xLoc = x;
	}

	public void setAirportY(double y) {
		this.yLoc = y;
	}

}
